import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BienvenidadComponent } from './componts/bienvenidad/bienvenidad.component';
import { LeerqrComponent } from './componts/leerqr/leerqr.component';
import { DescargueComponent } from './componts/descargue/descargue.component';

@NgModule({
  declarations: [
    AppComponent,
    BienvenidadComponent,
    LeerqrComponent,
    DescargueComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
