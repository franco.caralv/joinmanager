import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BienvenidadComponent } from './componts/bienvenidad/bienvenidad.component';
import { LeerqrComponent } from './componts/leerqr/leerqr.component';
import { DescargueComponent } from './componts/descargue/descargue.component';

const routes: Routes = [
  {
    path: '',
    component: BienvenidadComponent,
  },
  {
    path: 'movil',
    component: LeerqrComponent,
  },
  {
    path: 'pc',
    component: DescargueComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled',
    useHash: true,
    anchorScrolling: 'enabled',
    scrollPositionRestoration: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
