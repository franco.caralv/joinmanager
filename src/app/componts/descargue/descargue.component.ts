import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-descargue',
  templateUrl: './descargue.component.html',
  styleUrls: ['./descargue.component.scss']
})
export class DescargueComponent implements OnInit {

  public imagen1 = "https://www.restaurant.pe/wp-content/uploads/2020/12/cropped-cropped-restaurant.pe_-1.png";

  public imagen3 = "assets/laptop.png";

  public imagen2 = "assets/boton1.png";

  constructor() { }

  ngOnInit(): void {
  }

  descargarpc() {
    window.open("https://web.restaurant.pe/QuipuNetInstaller-x64.exe", '_blank')
  }

}
