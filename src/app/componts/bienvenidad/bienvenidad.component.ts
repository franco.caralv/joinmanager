import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bienvenidad',
  templateUrl: './bienvenidad.component.html',
  styleUrls: ['./bienvenidad.component.scss']
})
export class BienvenidadComponent implements OnInit {


  public imagen1 = "https://www.restaurant.pe/wp-content/uploads/2020/12/cropped-cropped-restaurant.pe_-1.png";

  public urlrouterpc = "pc";

  public urlroutermovil = "movil";

  public imagen2 = "assets/boton1.png";

  public imagen3 = "assets/boton2.png";

  constructor() { }

  ngOnInit(): void {
  }

}
