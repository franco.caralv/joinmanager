import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BienvenidadComponent } from './bienvenidad.component';

describe('BienvenidadComponent', () => {
  let component: BienvenidadComponent;
  let fixture: ComponentFixture<BienvenidadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BienvenidadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BienvenidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
