import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LeerqrComponent } from './leerqr.component';

describe('LeerqrComponent', () => {
  let component: LeerqrComponent;
  let fixture: ComponentFixture<LeerqrComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LeerqrComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LeerqrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
