const colors = require('tailwindcss/colors');

module.exports = {
  mode: 'jit',
  purge: {
    enabled: true,
    content: ['./src/**/*.{html,scss,ts}']
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        'body-base': '#566271',
        'body-dark': '#454E5A',
        'control': '#e1e3e5',
        'link': colors.blue[500],
        'separator': colors.gray[200],
      }
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
